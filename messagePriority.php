<?php

class RabbitMQ_MessagePriority {

    public static function _get($message, $queue_name) {
        if (strpos($queue_name, 'property_es_shard') !== false) {
            $priority = 2;
            if (isset($message['case'])) {
                $case = $message['case'];
                switch ($case) {
                    case 'immediate':
                        $priority = 9;
                        break;
                    case 'bulk':
                        $priority = 8;
                        break;
                    case 'bulk_import':
                        $priority = 7;
                        break;
                    case 'bulk_api':
                        $priority = 6;
                        break;
                    case 'bulk_24':
                        $priority = 4;
                        break;
                    case 'unset':
                        $priority = 9;
                        break;
                    case 'bulk_segmentation':
                        $priority = 5;
                        break;
                    case 'update':
                        $priority = 9;
                        break;
                    case 'insert':
                        $priority = 9;
                        break;
                    case 'nested_bulk':
                        $priority = 1;
                        break;
                    case 'nested_gcm_apn':
                        $priority = 6;
                        break;
                    default:
                        $priority = 2;
                        break;
                }
            }
            return $priority;
        } else {
            if (isset($message['projectId'])) {
                $projectId = $message['projectId'];
            } elseif (isset($message['pr'])) {
                $projectId = $message['pr'];
            } else {
                $projectId = 0;
            }

            switch ($projectId) {
                case 30036:
                    $priority = 1;
                    break;
                case 30268:
                    $priority = 1;
                    break;
                case 33902:
                    $priority = 1;
                    break;
                case 30045:
                    $priority = 2;
                    break;
                case 30007:
                    $priority = 3;
                    break;
                case 30004:
                    $priority = 5;
                    break;
                case 30163:
                    $priority = 6;
                    break;
                case 30167:
                    $priority = 3;
                    break;
                case 30172:
                case 30307:
                    $priority = 4;
                    break;
                case 33314:
                    $priority = 7;
                    break;
                case 30009:
                    $priority = 9;
                    break;
                default:
                    $priority = 8;
                    break;
            }

            return $priority;
        }
    }
}
