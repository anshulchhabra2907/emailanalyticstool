filepickerUploadFn = function (params) {
    params = params || {};
    var bucketPath = params.bucketPath || '';
    var accessType = params.accessType || 'private';
    var exts = params.exts || '';
    var mimetypes = params.mimetypes || '';
    var maxFiles = params.maxFiles || 1;
    var maxSize = params.maxSize || 200 * 1024 * 1024;
    var buttonName = params.buttonName;

    if (bucketPath == '') {
        alert('Invalid call, Please check your parameter supplied !');
        return false;
    }

    var newMimetypes = [];
    if (mimetypes != ''){
        $.each(mimetypes.split(','), function (m, mVal) {
            if (mVal != '') {
                newVal = '';
                if (mVal.indexOf('/*') > 2) {
                    newVal = mVal;
                }
                if (mVal.indexOf('/') <= 0) {
                    newVal = mVal + '/*';
                } else if (mVal.indexOf('/') > 0) {
                    newVal = mVal;
                }
                if (newVal != '') {
                    newMimetypes.push(newVal);
                }
            }
        });
    }
    var newExts = [];
    if (newMimetypes.length <= 0) {
        exts = exts || 'txt,pdf,doc,docx,xls,xlsx,csv,jpg,png,gif';
        $.each(exts.split(','), function (i, val) {
            if (val != '') {
                newExts.push(val.substr(0, 1) == '.' ? val : '.' + val);
            }
        });
        if (newExts.length == 0) {
            newExts = ['.txt', '.pdf', '.doc', '.docx', '.xls', '.xlsx', '.csv', '.jpg', '.png', '.gif'];
        }
    }
    var param = {folders: true, multiple: false, maxFiles: maxFiles, extensions: newExts, maxSize: maxSize};
    if (newMimetypes.length > 0) {
        param = {mimetype: newMimetypes, folders: true, multiple: false, maxFiles: maxFiles};
    }
    if (maxFiles > 1) {
        param.multiple = true;
    }
    // set the key
    filepicker.setKey("A4fSX9sbtR3XlPa14Jiufz");
    // change the button text 
    $('button[name="' + buttonName + '"]').text("processing...");
    // start processing the multiple files
    filepicker.pickMultiple(param, function (files) {
        $.each(files, function (index, file) {
            var filename = file.filename;
            var container = file.container;
            if (!container) {
                container = $('#bucketname').val();
            }
            var new_name = filename.replace(/\ /g, '-');
            // after saving the file chnage the file access type
            filepickerParams = {location: 'S3', access: accessType, path: bucketPath, filename: new_name};
            filepicker.store(file, filepickerParams,function (store_resp){
                var custom_url = 'https://s3.amazonaws.com/' + container + '/' + store_resp.key;
                $('#csv_url').val(custom_url);
            });
        });
    }, function (FPError) {
        $('button[name="' + buttonName + '"]').text("UPLOAD FILE");
        $("#appendFileName").text('');
        console.log(JSON.stringify(FPError));
    });
}