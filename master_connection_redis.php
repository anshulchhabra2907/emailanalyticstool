<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 8/5/17
 * Time: 8:10 PM
 */

//staging
define("REDIS_IP","94.130.9.112");
define("REDIS_PORT","7000");

////local
//define("REDIS_IP","127.0.0.1");
//define("REDIS_PORT","6379");

class RedisConnection
{
    function createConnection($ip=REDIS_IP,$port=REDIS_PORT)
    {
        try {
            $conn = new Redis();
            $conn->connect($ip,$port);
            return $conn;
        } catch (Exception $exception) {
            print_r($exception->getMessage());
        }
    }

    function closeConnection($conn)
    {
        $conn->close();
    }
}