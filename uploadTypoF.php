<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>TYPO UPLOAD</title>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"/>
</head>
<body>
<!--<div style="display: inline-block">-->
    <b>Invalid Domain Name List(seperated by single space(" ")):</b> <br><input style="width: 100%" id="domain_typo" type="text" >
<br>
<br>
<b>Reason:<br></b></b>
<select  id="reason">
    <option id="reason_domain_typo" value=3>Typo In Domain</option>
    <option id="reason_domain_risky" value=9>Risky Domain</option>
</select>
<br>
<br>
    <b>Can Be Domain Name:</b><br><input id="domain_original" type="text">
<br>
<marquee id="marquee_url" style="color: darkblue;width:170px">
    <a href="http://www.dcode.fr/typing-error-generator" target="_blank" onmouseover="onMouseOverMarqueeUrl('stop')" onmouseleave="onMouseOverMarqueeUrl('start')">Click here for more typos.</a>
</marquee>
<br>
<!--</div>-->
<br>

<div id="if_update_div">
    <input type="radio" name="if_update" id="update" checked>Update
    <input type="radio" name="if_update" id="delete" >Delete
</div>
<br>

<input style="background-color: darkblue;color: white" id="submit" type="submit" value="SUBMIT" onclick="uploadData()">

<br>
<br>
<br>

<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%"/>

</body>

<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        getTypoList();
    });

    function uploadData() {
        var typoDomain = document.getElementById("domain_typo").value;
        var reason = document.getElementById("reason").value;
        var originalDomain = document.getElementById("domain_original").value;
        if (typoDomain != "" && typoDomain != null && typoDomain != undefined && typoDomain.slice(-1) != "," && typoDomain.slice(-1) != " ") {
//            if (reason != "" && reason != null && reason != undefined && reason.slice(-1)!=",") {
            if (originalDomain != "" && originalDomain != null && originalDomain != undefined && originalDomain.slice(-1) != " " && originalDomain.slice(-1) != ",") {

                var isDelete = 0;
                if (document.getElementById("update").checked == true) {
                    isDelete = 0;
                } else {
                    isDelete = 1;
                }
                $.ajax({
                    //change this to uploadTypoB.php to run on local
//                    url: "uploadTypoB.php",
                    url: "uploadTypoB",
                    data: {
                        "require": "update_typo_list"
                        , "is_delete": isDelete
                        , "typo_domain": typoDomain
                        , "reason": reason
                        , "original_domain": originalDomain
                    },
                    async: false,
                    success: function (response) {
                        if (response != 0) {
                            alert("successfully updated");
                            getTypoList();
                        }
                    }
                });
            } else {
                alert("Please enter correct Can-Be-Domain");
            }
//        } else {
//            alert("");
//        }
        } else {
            alert("Please enter correct Invalid domain name");
        }
    }

    function getTypoList() {
        $.ajax({
            //change this to uploadTypoB.php to run on local
//            url: "uploadTypoB.php",
            url: "uploadTypoB",
            data: {
                "require": "get_typo_list"
            },
            async: false,
            success: function (typoListJson) {
                if (typoListJson != 0) {
                    typoListArray = JSON.parse(typoListJson);
                    displayData(typoListArray);
                }
            }
        });
    }

    function displayData(detailedData) {
        $('#example').DataTable({
            destroy: true,
            data: detailedData,
            dom: 'Bfrtip',
            pageLength: 50,
            buttons: [
                'copy'
                , 'csv'
                , 'excel'
                , 'pdf'
//                ,'print'
            ],
            columns: [
                {title: "Typo Domain"},
                {title: "Reason"},
                {title: "Can Be Domain"}
            ]
        });
    }

    function onMouseOverMarqueeUrl(status) {
        if(status=="stop") {
            document.getElementById('marquee_url').stop();
        }else if(status=="start"){
            document.getElementById('marquee_url').start();
        }
    }


</script>
</html>
