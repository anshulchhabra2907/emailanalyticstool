<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 8/5/17
 * Time: 7:32 PM
 */

require_once('master_connection_rabbitmq.php');
require_once('master_connection_mongo.php');
require_once('logger.php');


class toFilterFromDbWorkerD extends RabbitMqConnection
{

    public function __construct($queue)
    {
        parent::$_queue = $queue;
    }

    protected function _process($mailData)
    {
        try {
            print_r($mailData);
            $collectionName = $mailData['collection_name'];
            $emailList = $mailData['mail_list'];
            $ifSmtp = $mailData['if_smtp'];
            $ifLast = $mailData['end'];
            $this->filterVerifiedEmail($emailList, $collectionName,$ifSmtp,$ifLast);
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    function filterVerifiedEmail($emailList, $collectionName,$ifSmtp,$ifLast)
    {
        $conn = $this->getMongoConnection();
//    $totalCollectionSize = $conn->count();
//    $mongoDocChunks = ceil($totalCollectionSize/1000);
        $nonVerifiedEmailList = array();
        $matchedEmailList = array();

        foreach ($emailList as $k=>$emailInfo) {
            $emailIdArrayToFind=array('email_id'=>$emailInfo['email_id']);
            $tempArray=array();
            $tempArray = $emailInfo;
            $emailCut = explode("@", $tempArray['email_id']);
            $conn->setCollection("verified_mail_list_master");
            $ifExistsInVerified = $conn->find($emailIdArrayToFind);
            if (!empty($ifExistsInVerified)) {
                $tempArray['result']='verified';
                $tempArray['reason']='1';
                $tempArray['score']='5';
                $tempArray['user']=$emailCut[0];
                $tempArray['domain']=$emailCut[1];
                array_push($matchedEmailList , $tempArray);
            } else {
                $conn->setCollection("bounced_mail_list_master");
                $ifExistsInBounced = $conn->find($emailIdArrayToFind);
                if (!empty($ifExistsInBounced)) {
                    $tempArray['result']='bounced';
                    $tempArray['reason']='1';
                    $tempArray['score']='0';
                    $tempArray['user']=$emailCut[0];
                    $tempArray['domain']=$emailCut[1];
                    array_push($matchedEmailList , $tempArray);
                } else {
                    $tempArray['result']='to_verify';
                    $tempArray['reason']='5';
                    array_push($nonVerifiedEmailList, $tempArray);
                }
            }
        }
        //sendDataToMongoViaWorker
        if (count($matchedEmailList ) != 0) {
            $queue_name = "mail_id_insert";
            $this->sendDataToQueue($matchedEmailList , $collectionName, $queue_name,$ifSmtp,$ifLast);
        }
        //sendDataToQueueForMXCheck
        if (count($nonVerifiedEmailList) != 0) {
            $queue_name = "mail_id_mx";
            $this->sendDataToQueue($nonVerifiedEmailList, $collectionName, $queue_name,$ifSmtp,$ifLast);
        }
    }

    function sendDataToQueue($EmailList, $collectionName, $queue_name,$ifSmtp,$ifLast)
    {
        $finalArray['collection_name'] = $collectionName;
        $finalArray['mail_list'] = $EmailList;
        $finalArray['if_smtp'] = $ifSmtp;
        $finalArray['end'] = $ifLast;

        $conn = new RabbitMqConnection();
        $conn->_produce($finalArray,$queue_name);

        if($ifLast=="1"){
//            $this->sendMail("DB FILTER CHECK DONE!",$collectionName);
        }
    }

    function sendMail($message,$collection_name){

        $headers = "From:EMAIL_REPORT <betaout.errors@gmail.com>\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";
        $headers .= "Content-Transfer-Encoding:base64 \r\n";
        $messagebody= $base64contents = rtrim(chunk_split(base64_encode($message)));

        mail("betaout.errors@gmail.com,anshul@betaoutinc.com", "EMAIL_REPORT COLLECTION:".$collection_name , $messagebody, $headers);
    }

    function getMongoConnection()
    {
        $conn = new MongoConnection();
        return $conn;
    }
}