<?php
class CsvImporter
{
    private $fp;
    private $parse_header;
    private $header;
    private $delimiter;
    private $length;
    private $emailIndex;
    private $isFilter;
    //--------------------------------------------------------------------
    function __construct($isFilter,$file_name, $parse_header=false, $delimiter="\t", $length=8000)
    {

        try {
            $this->fp = fopen($file_name, "r");
            $this->parse_header = $parse_header;
            $this->delimiter = $delimiter;
            $this->length = $length;
            $this->emailIndex = -1;
            $this->isFilter = $isFilter;
            //$this->lines = $lines;
            if ($this->parse_header) {
                $this->header = fgetcsv($this->fp, $this->length, $this->delimiter);
            }
            $this->emailIndex = array_search(strtolower("emaIl"), array_map('strtolower', explode(",", $this->header[0])));
        }catch (Exception $exception){
            error_log($exception->getMessage());
        }
    }
    //--------------------------------------------------------------------
    function ifEmailExists()
    {
        if ($this->emailIndex >(-1))
            return 1;
        else
            return 0;
    }

    function __destruct()
    {
        if ($this->fp)
        {
            fclose($this->fp);
        }
    }
    //--------------------------------------------------------------------
    function get($max_lines=0)
    {
        //if $max_lines is set to 0, then get all the data

        $data = array();
        $emailInfo = array();

        if ($max_lines > 0)
            $line_count = 0;
        else
            $line_count = -1; // so loop limit is ignored
        while ($line_count < $max_lines && ($row = fgetcsv($this->fp, $this->length, $this->delimiter)) !== FALSE)
        {
            if ($this->parse_header)
            {
                foreach ($this->header as $i => $heading_i)
                {
                    $emailId=strtolower(explode(',', $row[$i])[$this->emailIndex]);
                    if($this->isFilter) {
                        $emailInfo["email_id"] = $emailId;
                        $emailInfo["result"] = '';
                        $emailInfo["reason"] = '';
                        $emailInfo["typo"] = '';
                        $emailInfo["mx"] = '';
                        $emailInfo["disposable"] = '';
                        $emailInfo["smtp"] = '';
                        $emailInfo["did_you_mean"] = '';
                        $emailInfo["score"] = '';
                        $emailInfo["user"] = '';
                        $emailInfo["domain"] = '';
                    }else{
                        $emailInfo["email_id"] = $emailId;
                    }
                }
                array_push($data,$emailInfo);
            }
            if ($max_lines > 0)
                $line_count++;
        }
        if($data) {
            $end = "0";
            if($line_count < $max_lines){//random check if csv read complete
                $end="1";
            }
            $data['end'] =$end;
            return $data;
        }
    }
    //--------------------------------------------------------------------

}