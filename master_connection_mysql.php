<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 8/5/17
 * Time: 8:10 PM
 */

define("DB_NAME","mailAnayticLog");
define("IP","127.0.0.1:3306");
define("USER","root");
define("PASS","root");
//define("IP","mongodb://stag-push.betaout.in:27017");


class MysqlConnection
{
    function createConnection($ip=IP,$user=USER,$pass=PASS)
    {
        try {
            $conn = new mysqli($ip,$user,$pass);
            return $conn;
        }catch (Exception $exception){
        }
    }
    function selectDatabase($conn,$dbName=DB_NAME){
        $db = $conn->select_db($dbName);;
        return $db;
    }

    function closeConnection($conn)
    {
        $conn->close();
    }
}