<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>IMPORT MAIL LIST</title>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css"/>
</head>
<body>

<input type="radio" name="filter" id="radio_filter" onclick="onClickFilter('filter') checked">Filter
<input type="radio" name="filter" id="radio_import" onclick="onClickFilter('import')" >Import

<br>
<br>
<input type="radio" name="upload" id="radio_file" onclick="checkIfFile('file')">File
<input type="radio" name="upload" id="radio_url" onclick="checkIfFile('url')">URL

<br><br>
<div id="if_verified_div" style="visibility: hidden">
    <input type="radio" name="if_verified" id="radio_verified">Verified List
    <input type="radio" name="if_verified" id="radio_bounced" >Bounced List
</div>
<br><br>
<div id="div_url" style="visibility: hidden;height: auto">
    <b><a href="javascript:void(0);" onclick="onClickFileUpload()" >CLICK HERE TO GENERATE URL</a></b>
    <br><br><br>
    <b>CSV Download Url :</b> <input id="csv_url" type="url" placeholder="csv downlaod url" style="width:100%"><br>
<br>
    <div id="coll_name_div">
    <b>Collection Name : </b> <input id="coll_name" type="text" placeholder="Default will be current date and time" style="width:300px">
    </div>
<!--    <input type="checkbox" name="google_sharable_url" id="google_sharable_url" onchange="onChangeGoogleSharableCheckBox()">Google Sharable Link-->
    <br><br><b>Note: There must be an Email column in the CSV file.</b>
    <div id="if_smtp_div" style="visibility: hidden">
        <input type="checkbox" name="if_smtp" id="if_smtp" >SMTP Verification
    </div>

</div>

<div id="div_file" style="visibility: hidden;height: auto">
    Select File : <input type="file" name="file" id="csv_file"/>
    <br><br><b>Note: There must be an Email column in the CSV file.<br>
        This feature is removed. kindly genarate downloadable url from S3 Server or google drive.</b></br>
</div>

<br><br><br>

<input id="submit" type="submit" style="visibility: hidden" onclick="checkUrlValidationAndImportCSV()">

<br><br><br>

<img id="loader_image" style="visibility: hidden;width: 100%;height: 50px;background-color: antiquewhite;align-self: center;" src="http://2.bp.blogspot.com/-O7nsXfmgwSc/T6PQ0PVr6-I/AAAAAAAAAQI/-eXkEXj24-s/s1600/02.gif"><p id="response" style="visibility: hidden">Response:</p>

</body>

<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://api.filestackapi.com/filestack.js"></script>

<script type="text/javascript">
    function checkUrlValidationAndImportCSV() {
        var error = false;
        document.getElementById("response").innerHTML = "Response : ";
        document.getElementById("response").style.visibility = "visible";
        var type = 'file';
        if(document.getElementById("radio_url").checked == true){
            type = 'url';
        }
        var data="";
        if(type == 'url'){
            data = document.getElementById("csv_url").value;
//            if(document.getElementById("google_sharable_url").checked == true){
//                data = "https://drive.google.com/uc?export=download&id="+data;
//            }
        }else{
//        data = document.getElementById("csv_file").value
        }
        var require = "filter_data";
        var coll_name = document.getElementById("coll_name").value;
        if(coll_name == ""){
            coll_name = Math.floor(Date.now() / 1000);
        }else if (coll_name.indexOf(' ') >= 0){
            alert("Please enter collection name without spaces!");
            error=true;
        }
        if(document.getElementById("radio_filter").checked == true){
            require = "filter_data";
        }else{
            require = "import_data";
        }
        var list_name = "";
        if(document.getElementById("radio_verified").checked == true){
            list_name = "verified";
        }else{
            list_name = "bounced";
        }

        var if_smtp = "0";
//        if(document.getElementById("if_smtp").checked == true){
//            if_smtp = "1";
//        }else{
//            if_smtp = "0";
//        }

        if(data == "") {
            error = true;
        }
        if(!error){
            var conf = 1;
            if(document.getElementById("radio_import").checked == true) {
                var conf = confirm("Are you sure you want to upload list as "+list_name+"?");
            }
            if(conf) {
                $.ajax({
                    //change this to uploadcsvB.php to run on local
//                    url: "uploadcsvB.php",
                    url: "uploadcsvB",
                    data: {
                        "require": require,
                        "type": type,
                        "data": data,
                        "coll_name": coll_name,
                        "if_smtp": if_smtp,
                        "list_name": list_name
                    },
                    beforeSend: function () {
                        document.getElementById("loader_image").style.visibility = "visible";
                    },
                    success: function (response) {
                        document.getElementById("response").innerHTML = "Response : " + response;
                        document.getElementById("loader_image").style.visibility = "hidden";
                    }
                });
            }else{
                        document.getElementById("response").innerHTML = "Response : List upload cancelled!" ;
            }
        }
    }

    function checkIfFile(id){
        document.getElementById("loader_image").style.visibility = "hidden";
        document.getElementById("loader_image").style.visibility = "hidden";
        document.getElementById("response").innerHTML = "";
        document.getElementById("response").style.visibility = "hidden";
        if(id=="file"){
            document.getElementById("submit").style.visibility = "hidden";
            document.getElementById("div_file").style.visibility = "visible";
            document.getElementById("div_url").style.visibility = "hidden";
            document.getElementById("coll_name_div").style.visibility = "hidden";
            document.getElementById("if_smtp_div").style.visibility = "hidden";
        }else{
            document.getElementById("submit").style.visibility = "visible";
            document.getElementById("div_url").style.visibility = "visible";
            document.getElementById("div_file").style.visibility = "hidden";
            if(document.getElementById("radio_filter").checked == true) {
                document.getElementById("coll_name_div").style.visibility = "visible";
                document.getElementById("if_smtp_div").style.visibility = "visible";
            }
        }
        if(document.getElementById("radio_import").checked == true) {
            document.getElementById("if_verified_div").style.visibility = "visible";
            document.getElementById("radio_verified").checked = true;
        }
    }

    function tempAlert(msg,duration)
    {
        var el = document.createElement("div");
        el.setAttribute("style","position:absolute;top:40%;left:20%;background-color:white;");
        el.innerHTML = msg;
        setTimeout(function(){
            el.parentNode.removeChild(el);
        },duration);
        document.body.appendChild(el);
    }

//    function onChangeGoogleSharableCheckBox(){
//        if(document.getElementById("google_sharable_url").checked == true){
//            document.getElementById("csv_url").placeholder = "Add only random-generated-Id from google sharable link";
//            alert("Add only random-generated-Id from google sharable link. It looks like: 0B6RVtJJMtQAsd0h1ZkROtgyucVE ");
//        }else{
//            document.getElementById("csv_url").placeholder = "csv downlaod url";
//        }
//    }

    function onClickFilter(id){
        if(id=='import'){
            document.getElementById("coll_name_div").style.visibility = "hidden";
            document.getElementById("if_smtp_div").style.visibility = "hidden";
            document.getElementById("if_verified_div").style.visibility = "visible";
        }else{
            document.getElementById("coll_name_div").style.visibility = "visible";
            document.getElementById("if_smtp_div").style.visibility = "visible";
            document.getElementById("if_verified_div").style.visibility = "hidden";
        }
        document.getElementById("radio_url").checked = true;
        checkIfFile("url");
    }


    function onClickFileUpload(){
            var dt = new Date();
            var date = dt.getFullYear() + "/" + (dt.getMonth() + 1) ;
            var bucketPath = "email_filter/fileimport/" + date + "/";
            var accessType = 'public';
            // var mimetypes = "*";
            var maxFiles = 1;
            var exts = 'csv';
            var maxSize = 200 * 1024 * 1024;
            var submitButtonName = $('button[name="submitPicker"]').attr("name");

            var param = {
                bucketPath: bucketPath, accessType: accessType, maxFiles: maxFiles, exts: exts, maxSize: maxSize, buttonName: submitButtonName};
            filepickerUploadFn(param);
    }


    filepickerUploadFn = function (params) {
        params = params || {};
        var bucketPath = params.bucketPath || '';
        var accessType = params.accessType || 'private';
        var exts = params.exts || '';
        var mimetypes = params.mimetypes || '';
        var maxFiles = params.maxFiles || 1;
        var maxSize = params.maxSize || 200 * 1024 * 1024;
        var buttonName = params.buttonName;

        if (bucketPath == '') {
            alert('Invalid call, Please check your parameter supplied !');
            return false;
        }

        var newMimetypes = [];
        if (mimetypes != ''){
            $.each(mimetypes.split(','), function (m, mVal) {
                if (mVal != '') {
                    newVal = '';
                    if (mVal.indexOf('/*') > 2) {
                        newVal = mVal;
                    }
                    if (mVal.indexOf('/') <= 0) {
                        newVal = mVal + '/*';
                    } else if (mVal.indexOf('/') > 0) {
                        newVal = mVal;
                    }
                    if (newVal != '') {
                        newMimetypes.push(newVal);
                    }
                }
            });
        }
        var newExts = [];
        if (newMimetypes.length <= 0) {
            exts = exts || 'txt,pdf,doc,docx,xls,xlsx,csv,jpg,png,gif';
            $.each(exts.split(','), function (i, val) {
                if (val != '') {
                    newExts.push(val.substr(0, 1) == '.' ? val : '.' + val);
                }
            });
            if (newExts.length == 0) {
                newExts = ['.txt', '.pdf', '.doc', '.docx', '.xls', '.xlsx', '.csv', '.jpg', '.png', '.gif'];
            }
        }
        var param = {folders: true, multiple: false, maxFiles: maxFiles, extensions: newExts, maxSize: maxSize};
        if (newMimetypes.length > 0) {
            param = {mimetype: newMimetypes, folders: true, multiple: false, maxFiles: maxFiles};
        }
        if (maxFiles > 1) {
            param.multiple = true;
        }
        // set the key
        filepicker.setKey("A4fSX9sbtR3XlPa14Jiufz");
        // change the button text
        $('button[name="' + buttonName + '"]').text("processing...");
        // start processing the multiple files
        filepicker.pickMultiple(param, function (files) {
            $.each(files, function (index, file) {
                var filename = file.filename;
                var container = file.container;
                if (!container) {
                    container = $('#bucketname').val();
                }
                var new_name = filename.replace(/\ /g, '-');
                // after saving the file chnage the file access type
                filepickerParams = {location: 'S3', access: accessType, path: bucketPath, filename: new_name};
                filepicker.store(file, filepickerParams,function (store_resp){
                    var custom_url = 'https://s3.amazonaws.com/' + container + '/' + store_resp.key;
                    $('#csv_url').val(custom_url);
                });
            });
        }, function (FPError) {
            $('button[name="' + buttonName + '"]').text("UPLOAD FILE");
            $("#appendFileName").text('');
            console.log(JSON.stringify(FPError));
        });
    }


</script>
</html>