<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/5/17
 * Time: 3:31 PM
 */
require_once 'master_connection_rabbitmq.php';
require_once 'CSVImporter.php';

if ($_REQUEST["require"] == ("import_data" || "filter_data")) {
//    $url = '/home/hp/Documents/user_user_170505124444.csv';
    $type = $_REQUEST["type"];
    $data = $_REQUEST["data"];
    $ifSmtp = $_REQUEST["if_smtp"];
    $collection_name = "";
    if ($_REQUEST["require"] == "import_data") {
        $queue_name = 'mail_id_insert';
        $isFilter = 0;
        if ($_REQUEST["list_name"] == "verified")
            $collection_name = "verified_mail_list_master";
        else
            $collection_name = "bounced_mail_list_master";
    } else {
        $isFilter=1;
        $queue_name = 'mail_id_filter';
        $collection_name = $_REQUEST["coll_name"];
    }
    try {
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', 30000); //30000 seconds = 500 minutes

        if ($type == 'url') {
            $importer = new CsvImporter($isFilter,$data, true);
            if ($importer->ifEmailExists()) {
                $count = 0;
                while ($emailListArray = $importer->get(1000)) {
                    $count+=1;
                    initiateMailQueue($emailListArray,$queue_name,$collection_name,$ifSmtp,$count);
                }
                echo $count." Queues generated :)";
            } else {
                echo "Email column is not there in the attached csv.";
            }
        }
    } catch (Exception $e) {
        echo $e . get_call_stack();
    }
}
function initiateMailQueue($mailIdInfoArray,$queue_name,$collection_name,$ifSmtp,$count)
{
    $ifEnd = $mailIdInfoArray['end'];
    unset($mailIdInfoArray['end']);
    $finalArray = array();
    $finalArray['collection_name'] = $collection_name;
    $finalArray['mail_list'] = $mailIdInfoArray;
    $finalArray['if_smtp'] = $ifSmtp;
    $finalArray['end'] = $ifEnd;

    $conn = new RabbitMqConnection();
    $conn->_produce($finalArray,$queue_name);

    if($ifEnd == "1"){
//        sendMail($count,"QUEUES INITIATED",$collection_name);
    }
}

function sendMail($count,$message,$collection_name){

    $headers = "From:EMAIL_REPORT <betaout.errors@gmail.com>\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=utf-8\r\n";
    $headers .= "Content-Transfer-Encoding:base64 \r\n";
    $messagebody= $base64contents = rtrim(chunk_split(base64_encode($count." ".$message)));

    mail("betaout.errors@gmail.com,anshul@betaoutinc.com", "EMAIL_REPORT COLLECTION:".$collection_name , $messagebody, $headers);
}
