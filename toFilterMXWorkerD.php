<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 8/5/17
 * Time: 7:32 PM
 */

require_once('master_connection_rabbitmq.php');
require_once('master_connection_mongo.php');
require_once('master_connection_redis.php');
require_once('Constants.php');
require_once('logger.php');
require __DIR__.'/vendor/autoload.php';
use EmailChecker\EmailChecker;

class ToFilterMXWorkerD extends RabbitMqConnection
{

    public function __construct($queue)
    {
        parent::$_queue = $queue;
    }

    protected function _process($mailData)
    {
        try {
//            print_r($mailData);
            $collectionName = $mailData['collection_name'];
            $emailList = $mailData['mail_list'];
            $ifSmtp = $mailData['if_smtp'];
            $ifLast = $mailData['end'];
            $this->filterVerifiedEmail($emailList, $collectionName,$ifSmtp,$ifLast);
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    function filterVerifiedEmail($emailList, $collectionName,$ifSmtp,$ifLast)
    {
        $toVerifyEmailList = array();// mail id verified and further verification(SMTP) needed
        $verifiedEmailList = array();//mail id verified and failed at any step, so no further verification needed

        $conn_redis = $this->getRedisConnection(14);
        foreach ($emailList as $emailInfo) {
            $isValidStatusWithInfoArray = $this->isEmailValid($emailInfo, $conn_redis);

//            $isValidStatusWithInfoArray['email_info'] will have Updated email Info
//            $isValidStatusWithInfoArray['status'] will be used to check if all passed for further smtp validation or failed in any existing validation
//            echo json_encode($isValidStatusWithInfoArray);
            if ($isValidStatusWithInfoArray['status'] == 1) {
                array_push($toVerifyEmailList, $isValidStatusWithInfoArray['email_info']);
            } else {
                array_push($verifiedEmailList, $isValidStatusWithInfoArray['email_info']);
            }
        }
        $this->closeRedisConnection($conn_redis);
        //sendDataToMongo
        if (count($verifiedEmailList) != 0) {
            $queue_name = "mail_id_insert";
            $this->sendDataToQueue($verifiedEmailList, $collectionName, $queue_name,$ifLast);
        }

        //sendDataToQueueForSMTP
        if (count($toVerifyEmailList) != 0) {
            if($ifSmtp=="1") {
                $queue_name = "mail_id_smtp";
                $this->sendDataToQueue($toVerifyEmailList, $collectionName, $queue_name,$ifLast);
            }else {
                $queue_name = "mail_id_insert";
                $this->sendDataToQueue($toVerifyEmailList, $collectionName, $queue_name, $ifLast);
            }
            $queue_name = "mail_id_insert";
            $this->sendDataToQueue($toVerifyEmailList, $collectionName . "_toverify", $queue_name, $ifLast);
        }
    }

    function sendDataToQueue($verifiedEmailList, $collectionName, $queue_name,$ifLast)
    {
        $finalArray['collection_name'] = $collectionName;
        $finalArray['mail_list'] = $verifiedEmailList;
        $finalArray['end'] = $ifLast;

        $conn = new RabbitMqConnection();
        $conn->_produce($finalArray,$queue_name);

        if($ifLast=="1"){
//            $this->sendMail("MX RECORDS CHECK DONE!",$collectionName);
        }
    }

    function isEmailValid($emailInfo,$conn_redis)
    {
        $typoCheckRes = $this->typoCheck($emailInfo);
        if ($typoCheckRes['status'] == 1) {
            $domainExistenceCheckRes = $this->domainExistenceCheck($typoCheckRes['email_info']);
//            echo "domain existance response is ".json_encode($domainExistenceCheckRes);
            if ($domainExistenceCheckRes['status'] == 1) {
                $mxCheckRes = $this->mxCheck($domainExistenceCheckRes['email_info'], 'MX', $conn_redis);
                if ($mxCheckRes['status'] == 1) {
                    $disposableEmailCheckRes = $this->disposableEmailCheck($mxCheckRes['email_info']);
                    return $disposableEmailCheckRes;
                } else {
                    return $mxCheckRes;
                }
            } else {
                return $domainExistenceCheckRes;
            }
        } else {
            return $typoCheckRes;
        }
    }

    function typoCheck($emailInfo)
    {
        $tempArray['email_info']=$emailInfo;
        if (filter_var($tempArray['email_info']['email_id'], FILTER_VALIDATE_EMAIL)
            && preg_match('/@.+\./', $tempArray['email_info']['email_id'])
        ) {
            $emailCut = explode("@", $tempArray['email_info']['email_id']);
            if (count($emailCut) == 2) {
                $tempArray['email_info']['user']=$emailCut[0];
                $tempArray['email_info']['domain']=$emailCut[1];
                if (strpos($emailCut[1], ".") !== false) {
                    $tempArray['email_info']['typo'] = 'true';
                    $tempArray['status']='1';
                    return $tempArray;
                } else {
                    $tempArray['email_info']['result'] = 'bounced';
                    $tempArray['email_info']['reason'] = '6';
                    $tempArray['email_info']['score'] = '0';
                    $tempArray['email_info']['typo'] = 'false';
                    $tempArray['status']='0';
                    return $tempArray;
                }
            } else {
                $tempArray['email_info']['result'] = 'bounced';
                $tempArray['email_info']['reason'] = '6';
                $tempArray['email_info']['score'] = '0';
                $tempArray['email_info']['typo'] = 'false';
                $tempArray['status']='0';
                return $tempArray;
            }
        } else {
            $tempArray['email_info']['result'] = 'bounced';
            $tempArray['email_info']['reason'] = '6';
            $tempArray['email_info']['score'] = '0';
            $tempArray['email_info']['typo'] = 'false';
            $tempArray['status']='0';
            return $tempArray;
        }
    }

    function domainExistenceCheck($emailInfo)
    {
        $tempArray['email_info']=$emailInfo;
        $emailCut = explode("@", $tempArray['email_info']['email_id']);
        $user = $emailCut[0];
        $domain = $emailCut[1];
        $conn = $this->getMongoConnection();
        $conn->setCollection("master_domain_typo_list");
        $filterArray = array("typo_domain"=>$domain);
        $res = $conn->find($filterArray);
        if (!empty($res)) {
            $tempArray['email_info']['result']=Constants::$result[$res[0]['reason']];
            $tempArray['email_info']['reason']=$res[0]['reason'];
            $tempArray['email_info']['score']=Constants::getScore($tempArray['email_info']['reason'],$tempArray['email_info']['result']);
            $tempArray['email_info']['user']=$user;
            $tempArray['email_info']['typo'] = 'false';
            $tempArray['email_info']['domain']=$domain;
            $tempArray['status'] = '0';
            if($tempArray['email_info']['reason'] == "3"){
                $tempArray['email_info']['did_you_mean'] = $user."@".$res[0]["can_be_domain"];
            }
            return $tempArray;
        } else {
            $tempArray['status'] = '1';
            return $tempArray;
        }
    }

    function mxCheck($emailInfo, $record = 'MX',$conn_redis)
    {
//        $tempArray=array();
        $tempArray['email_info']=$emailInfo;
        $domain = $tempArray['email_info']['domain'];
//        echo "-----domain is ----".$domain;
        if($conn_redis->exists($domain)){
            $mxResponse = $conn_redis->get($domain);
//            echo '--MX RESPONSE FROM REDIS IS-----'.$mxResponse;
        }else {
                $hosts = array();
                getmxrr($domain, $hosts);
//                echo count($hosts);
                if(count($hosts)==0 || $hosts[0]==""){
                    $mxResponse=0;
                }else{
                    $mxResponse=1;
                }
            $conn_redis->setex($domain,"15552000",$mxResponse);//180 day caching
//            echo '--MX RESPONSE FROM FUNCTION IS-----'.$mxResponse;
        }
//        echo '-'.$domain.'--MX RESPONSE IS-----'.$mxResponse;
        if($mxResponse){
            $tempArray['email_info']['result'] = 'to_verify';
            $tempArray['email_info']['score'] = '2';
            $tempArray['email_info']['mx'] = 'true';
            $tempArray['status'] = '1';
        }else{
            $tempArray['email_info']['result'] = 'bounced';
            $tempArray['email_info']['reason'] = '3';
            $tempArray['email_info']['score'] = '0';
            $tempArray['email_info']['mx'] = 'false';
            $tempArray['status'] = '0';
        }
        return $tempArray;
    }

    function disposableEmailCheck($emailInfo)
    {
//        $tempArray=array();
        $tempArray['email_info']=$emailInfo;

        $checker = new EmailChecker();

        if($checker->isValid($tempArray['email_info']['email_id'])){
            $tempArray['email_info']['result'] = 'to_verify';
            $tempArray['email_info']['score'] = '2.5';
            $tempArray['email_info']['disposable'] = 'true';
            $tempArray['status'] = '1';
        }else{
            $tempArray['email_info']['result'] = 'bounced';
            $tempArray['email_info']['reason'] = '4';
            $tempArray['email_info']['score'] = '0';
            $tempArray['email_info']['disposable'] = 'false';
            $tempArray['status'] = '0';
        }
        return $tempArray;
    }

    function sendMail($message,$collection_name){

        $headers = "From:EMAIL_REPORT <betaout.errors@gmail.com>\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";
        $headers .= "Content-Transfer-Encoding:base64 \r\n";
        $messagebody= $base64contents = rtrim(chunk_split(base64_encode($message)));

        mail("betaout.errors@gmail.com,anshul@betaoutinc.com", "EMAIL_REPORT COLLECTION:".$collection_name , $messagebody, $headers);
    }

    function getRedisConnection($dbIndex){
        $redis = new RedisConnection();
        $conn = $redis->createConnection();
//        $conn->select($dbIndex);
        return $conn;
    }

    function closeRedisConnection($conn){
        $redis = new RedisConnection();
        $redis->closeConnection($conn);
    }

    function getMongoConnection()
    {
        $conn = new MongoConnection();
        return $conn;
    }
}