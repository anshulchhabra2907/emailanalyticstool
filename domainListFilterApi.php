<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 26/5/17
 * Time: 1:49 PM
 */
require_once 'master_connection_rabbitmq.php';
require_once 'CSVImporter.php';

$require =$_REQUEST["require"];
if ($require == ("filter_domain")) {
    $finalFilteredDomainList = array();
    $type = $_REQUEST["type"];
    $data = $_REQUEST["data"];
    try {
        ini_set("memory_limit", -1);
        ini_set('max_execution_time', 30000); //30000 seconds = 500 minutes

        if ($type == 'url') {
            $importer = new CsvImporter($data, true);
            if ($importer->ifEmailExists()) {
                $filteredDomainList = array();
                $count = 0;
                while ($emailListArray = $importer->get(1000)) {
                    $count+=1;
                    filterDomainListAndUploadToMongo($emailListArray,count($emailListArray),$filteredDomainList);
                }
                echo $count." Queues generated :)";
            } else {
                echo "Email column is not there in the attached csv.";
            }
        }
    } catch (Exception $e) {
        echo $e . get_call_stack();
    }
}
function filterDomainListAndUploadToMongo($mailIdInfoArray,$count){
    global $finalFilteredDomainList;
    $finalFilteredDomainList = filterDomainList($mailIdInfoArray,$finalFilteredDomainList);
    if($count<1000){
        $queue_name = "mail_id_insert";
        $collection_name = $_REQUEST["coll_name"];
        initiateMailQueue($finalFilteredDomainList,$queue_name,$collection_name);
    }
}

function filterDomainList($mailIdInfoArray,$filteredDomainList)
{

    foreach ($mailIdInfoArray as $email) {
        $emailCut = explode("@", $email['email']);
        $domainName = strtolower($emailCut[1]);
        $ifDomainExists = 0;
        if (!empty($filteredDomainList)) {
            foreach ($filteredDomainList as $k=>$value) {
                if(isset($value[$domainName])){
                    $ifDomainExists = 1;
                    $filteredDomainList[$k][$domainName]['count']+=1;
                    break;
                }
            }
            if($ifDomainExists == 0){
                $domainInfo =array();
                $domainInfo[$domainName]['domain'] = $domainName;
                $domainInfo[$domainName]['count'] = 1;
                array_push($filteredDomainList, $domainInfo);
            }
        } else {
            $domainInfo =array();
            $domainInfo[$domainName]['domain'] = $domainName;
            $domainInfo[$domainName]['count'] = 1;
            array_push($filteredDomainList, $domainInfo);
        }
    }
    return $filteredDomainList;
}

function initiateMailQueue($mailIdInfoArray,$queue_name,$collection_name)
{
    $finalArray = array();
    $finalArray['collection_name'] = $collection_name;
    $finalArray['mail_list'] = $mailIdInfoArray;

    $conn = new RabbitMqConnection();
    $conn->_produce($finalArray,$queue_name);
}
