<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 8/5/17
 * Time: 7:32 PM
 */

require_once('master_connection_rabbitmq.php');
require_once('master_connection_mongo.php');
require_once('smtp_validateEmail.class.php');
require_once('logger.php');

class ToFilterSMTPWorkerD extends RabbitMqConnection
{

    public function __construct($queue)
    {
        parent::$_queue = $queue;
    }

    protected function _process($mailData)
    {
        try {
            print_r($mailData);
            $collectionName = $mailData['collection_name'];
            $emailList = $mailData['mail_list'];
            $ifLast = $mailData['end'];
            $this->filterVerifiedEmail($emailList, $collectionName,$ifLast);
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    function filterVerifiedEmail($emailList, $collectionName,$ifLast)
    {
        $verifiedEmailList = array();

        $SMTP_Validator = new SMTP_validateEmail();

        foreach ($emailList as $emailInfo) {
            $results = $SMTP_Validator->validate(array($emailInfo['email_id']), "anshulchhabra2009@gmail.com");
            print_r($emailInfo['email_id']);
            print_r("result--" . $results[$emailInfo['email_id']]);

            $tempArray=array();
            $tempArray = $emailInfo;

            if ($results[$emailInfo['email_id']]) {
                $tempArray['result']='verified';
                $tempArray['reason']='9';
                $tempArray['score']='4';
                $tempArray['smtp'] = 'true';
                array_push($verifiedEmailList,$tempArray);
            } else {
                $tempArray['result']='bounced';
                $tempArray['reason']='9';
                $tempArray['score']='0';
                $tempArray['smtp'] = 'false';
                array_push($verifiedEmailList,$tempArray);
            }

        }
        //sendDataToMongoViaWorker
        if (count($verifiedEmailList) != 0) {
            $queue_name = "mail_id_insert";
            $this->sendDataToQueue($verifiedEmailList, $collectionName."_smtp_verified", $queue_name,$ifLast);
        }
    }

    function sendDataToQueue($bouncedEmailList, $collectionName, $queue_name,$ifLast)
    {
        $finalArray['collection_name'] = $collectionName;
        $finalArray['mail_list'] = $bouncedEmailList;
        $finalArray['end'] = $ifLast;

        $conn = new RabbitMqConnection();
        $conn->_produce($finalArray, $queue_name);

        if($ifLast=="1"){
//            $this->sendMail("SMTP CHECK DONE!",$collectionName);
        }
    }

    function sendMail($message,$collection_name){

        $headers = "From:EMAIL_REPORT <betaout.errors@gmail.com>\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";
        $headers .= "Content-Transfer-Encoding:base64 \r\n";
        $messagebody= $base64contents = rtrim(chunk_split(base64_encode($message)));

        mail("betaout.errors@gmail.com,anshul@betaoutinc.com", "EMAIL_REPORT COLLECTION:".$collection_name , $messagebody, $headers);
    }
}