<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>IMPORT MAIL LIST</title>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css"/>
</head>
<body>

<br><br>
<div id="div_list_name" style="visibility: visible;height: auto">
    <input type="radio" name="radio_list" id="radio_list_all" onclick="onClickListFilter('all')" checked>All
    <input type="radio" name="radio_list" id="radio_list_verified" onclick="onClickListFilter('verified')" >Verified
    <input type="radio" name="radio_list" id="radio_list_bounced" onclick="onClickListFilter('bounced')" >Bounced
    <input type="radio" name="radio_list" id="radio_list_risky" onclick="onClickListFilter('risky')" >Risky
    <input type="radio" name="radio_list" id="radio_list_toverify" onclick="onClickListFilter('toverify')" >Still Remaining
    <b>Note: By default it is set for All. </b>
</div>

<br><br>

<div id="div_if_download" style="visibility: visible;height: auto">
    <input type="radio" name="radio_if_download" id="radio_count" onclick="onClickIfDownload('count')">Count
    <input type="radio" name="radio_if_download" id="radio_download" onclick="onClickIfDownload('download')" checked>Download csv
</div>

<br><br>
<div id="div_coll_name" style="visibility: visible;height: auto">
    <b>Note: Please write the collection name. </b>
    <br><br><b>Collection Name : </b> <input id="coll_name" type="text" placeholder="Example- Banglore_Report_bounced or Banglore_Report_verified" style="width:500px">
</div>

<br><br><br>

<input id="submit" type="submit" value="Download" style="visibility: visible" onclick="downloadCSV()">

<br>
<p id="response" style="visibility: hidden">Response:</p>

<br><br><br>

<img id="loader_image" style="visibility: hidden;width: 100%;height: 50px;background-color: antiquewhite;align-self: center; " src="http://2.bp.blogspot.com/-O7nsXfmgwSc/T6PQ0PVr6-I/AAAAAAAAAQI/-eXkEXj24-s/s1600/02.gif"><p id="response" style="visibility: hidden">Response:</p>

</body>

<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">
    toFilter = "all";
    ifDownload = "download";
    function downloadCSV(){
        var coll_name = getCollName();
        if(coll_name != 0){
            getcsv(coll_name);
        }else {
            errorFunction();
        }
    }

    function getcsv(coll_name){
        $.ajax({
            //change this to reportB.php to run on local
//            url: "reportB.php",
            url: "reportB",
            data: {
                "require": "csv",
                "coll_name":coll_name,
                "to_filter":toFilter,
                "if_download":ifDownload
            },
            beforeSend: function() {
                document.getElementById("loader_image").style.visibility = "visible";
            },
            async: false,
            success: function (data) {
                document.getElementById("loader_image").style.visibility = "hidden";
                if (data == "fail") {
                    alert("No data for collection name:" + coll_name + " exists.")
                } else {
                    if(ifDownload == 'count'){
                        document.getElementById("response").style.visibility = "visible";
                        document.getElementById("response").innerHTML = "Response : "+data;
                    }else {
                        var mongoDataArray = JSON.parse(data);
                        exportToCsv(coll_name + ".csv", mongoDataArray);
                    }
                }
            }
        });
    }

    function exportToCsv(filename, rows) {
        var processRow = function (row) {
            var finalVal = '';
            for (var j = 0; j < row.length; j++) {
                var innerValue = row[j] === null ? '' : row[j].toString();
                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                };
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }
            return finalVal + '\n';
        };
        var csvFile = '';
        for (var i = 0; i < rows.length; i++) {
            csvFile += processRow(rows[i]);
        }
        var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }

    function getCollName() {
        var coll_name = document.getElementById("coll_name").value;
        if (coll_name != "" && coll_name != null && coll_name != undefined) {
//            if (coll_name.indexOf("_bounced") !== -1) {
//                if (coll_name.split("_bounced")[0] != "") {
//                    return coll_name;
//                } else {
//                    return 0;
//                }
//            } else if (coll_name.indexOf("_verified") !== -1) {
//                if (coll_name.split("_verified")[0] != "") {
//                    return coll_name;
//                } else {
//                    return 0;
//                }
//            } else if (coll_name.indexOf("_risky") !== -1) {
//                if (coll_name.split("_risky")[0] != "") {
//                    return coll_name;
//                } else {
//                    return 0;
//                }
//            } else if (coll_name.indexOf("_toverify") !== -1) {
//                if (coll_name.split("_toverify")[0] != "") {
//                    return coll_name;
//                } else {
//                    return 0;
//                }
//            } else {
//                return coll_name + "_bounced";
//            }
            return coll_name;
        } else {
            return 0;
        }
    }

    function onClickListFilter(selectedFilterName){
        toFilter = selectedFilterName;
    }

    function onClickIfDownload(ifDownloadSelected){
        ifDownload = ifDownloadSelected;
    }

    function errorFunction(){
        alert("Please enter correct collection name");
    }


</script>
</html>