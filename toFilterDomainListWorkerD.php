<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 8/5/17
 * Time: 7:32 PM
 */

//ex- http://localhost/PhpStormProjects/EmailAnalyticsTool/domainListFilterApi.php?require=filter_domain&type=url&data=http%3A%2F%2Fwww.betaoutcdn.com%2F33962%2Ffileimport%2F2017%2F05%2F%20ac8Gpfg1T8KWelPvZnEb_Bounced-Latest.csv%20&coll_name=require=import_data&type=url&data=http%3A%2F%2Fwww.betaoutcdn.com%2F33962%2Ffileimport%2F2017%2F05%2FqEPdNnGQyuqWe8NvR4pQ_lis_users_withoptin_optout_toverify.csv&coll_name=lis_users_withoptin_optout_domain_list

require_once('master_connection_rabbitmq.php');
require_once('master_connection_mongo.php');
require_once('logger.php');


class ToFilterDomailListWorkerD extends RabbitMqConnection
{

    public function __construct($queue)
    {
        parent::$_queue = $queue;
    }

    protected function _process($mailData)
    {
        try {
            print_r($mailData);
            $collectionName = $mailData['collection_name'];
            $mailList = $mailData['mail_list'];
            $this->filterDomainList($mailList, $collectionName);
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    function filterDomainList($emailList, $collectionName)
    {
        $filteredDomainList = array();

        foreach ($emailList as $email) {
            $emailCut = explode("@", $email);
            $domainName = $emailCut[1];

            if (!in_array($domainName, $filteredDomainList)) {
                array_push($filteredDomainList,$domainName);
            }
        }
//        if (count($nonVerifiedEmailList) != 0) {
//            $queue_name = "mail_id_mx";
//            $this->sendDataToQueueForMXCheck($nonVerifiedEmailList, $collectionName, $queue_name);
//
////        $queue_name = "mail_id_insert";
////        $this->sendDataToMongoViaWorker($nonVerifiedEmailList,$collectionName."_nonverified",$queue_name);
//        }
    }

    function sendDataToMongoViaWorker($verifiedEmailList, $collectionName, $queue_name)
    {
        $finalArray['collection_name'] = $collectionName;
        $finalArray['mail_list'] = $verifiedEmailList;

        $conn = new RabbitMqConnection();
        $conn->_produce($finalArray,$queue_name);
    }

    function getMongoConnection()
    {
        $conn = new MongoConnection();
        return $conn;
    }
}