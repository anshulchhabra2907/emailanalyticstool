<?php

/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 6/6/17
 * Time: 12:05 PM
 */

class Constants{
    //score
    /**
     * @return array
     */
    /**
     * @return mixed
     */
    public static function getScore($reason,$result)
    {
        if($result == "verified"){
            if($reason == "1"){//matched
                return "5"; //score
            }else if($reason == "7"){ //smtp
                return "4";
            }
        }else if($result == "bounced"){
            return "0";
        }else if($result == "to_verify"){
            return "";
        }else if($result == "risky"){
            return "3";
        }
    }

    //Reason
    public static $reason = array(
        "matched" => "1",
        "typo" => "2",
        "typo_domain" => "3",
        "mx" => "4",
        "disposable" => "5",
        "n_matched" => "6",
        "smtp" => "7",
        "low_deliverability" => "8",
        "risky" => "9",
    );
//    public static $matched_reason_index = 1;//matched with db records
//    public static $typo_reason_index = 2; // typo in email
//    public static $typo_domain_reason_index = 3;//typo in email-domain-->gnail.com
//    public static $mx_reason_index = 4;
//    public static $disposable_reason_index = 5;
//    public static $n_matched_reason_index = 6; //not matched with db records-->result will be to_verify
//    public static $smtp_reason_index = 7;
//    public static $low_deliverability_reason_index = 8;
//    public static $risky_reason_index = 9;

    public static $result=array(
        "1"=>"verified",
        "2"=>"bounced",
        "3"=>"bounced",
        "4"=>"bounced",
        "5"=>"bounced",
        "6"=>"to_verify",
        "7"=>"bounced",
        "8"=>"risky",
        "9"=>"risky",
    );

}


?>