<?php
require_once 'master_connection_rabbitmq.php';
require_once 'CSVImporter.php';

use PhpAmqpLib\Message\AMQPMessage;

$importer = new CsvImporter("/home/hp/Documents/Bangalore_LIS_USER_DATA_25_apr.csv",true);
$csvFileName='Bangalore_LIS_USER_DATA_25_apr';

if(count($importer)>0) {
    $connection = getRabbitMqConnection();
    $channel = $connection->channel();
    $channel->queue_declare('mailQueue', false, false, false, false);

    $mailIdData = $importer->get(50);
    foreach ($mailIdData as $mailIdList) {
        genearteMailArrayAndInitiateQueue($mailIdList, $csvFileName,$channel);
    }
    $channel->close();
    $connection->close();
}

function genearteMailArrayAndInitiateQueue($mailIdDataArray,$csvFileName,$channel)
{
    $mailIdInfoArray = array();
    $mailIdInfoArray['email_list'] = $mailIdDataArray;
    $mailIdInfoArray['sender'] = 'anshulchhabra1991@gmail.com';
    $mailIdInfoArray['mongo_collection_name'] = $csvFileName;

    initiateMailQueue($mailIdInfoArray,$channel);
}
function initiateMailQueue($mailIdInfoArray,$channel){
    $msg = new AMQPMessage(json_encode($mailIdInfoArray));
    $channel->basic_publish($msg, '', 'mailQueue');
    echo "data sent---".json_encode($mailIdInfoArray);
}

function getRabbitMqConnection(){
    $rabbitMqConnection = new RabbitMqConnection();
    $conn = $rabbitMqConnection->createConnection();
    return $conn;
}

?>

