<?php

require_once 'master_connection_rabbitmq.php';
require_once('master_connection_mongo.php');
require_once('smtp_validateEmail.class.php');


$connection = getRabbitMqConnection();

$channel = $connection->channel();
$channel->queue_declare('mailQueue', false, false, false, false);

$callback = function($msg) {
//    echo " [x] Received ", $msg->body, "\n";
//    echo "verifying mail";
    $mailData = json_decode($msg->body,true);
//    echo "-----".json_encode($mailData);
    verifyEmail($mailData['email_list'],$mailData['sender'],$mailData['mongo_collection_name']);
};

$channel->basic_consume('mailQueue', '', false, true, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}

function verifyEmail($mailIdListArray,$sender,$collectionName)
{
    echo "-----1212".json_encode($mailIdListArray);
    $verifiedEmailDataForMongo=array();
    $bouncedEmailDataForMongo = array();

    foreach ($mailIdListArray as $mailContent) {
        $ifBounced=0;
        $email = $mailContent;

        $SMTP_Validator = new SMTP_validateEmail();
        $results = $SMTP_Validator->validate(array($email), $sender);

        if ($results[$email]) {
            $ifBounced =0;
            array_push($verifiedEmailDataForMongo, $email);
        } else {
            $ifBounced = 1;
            array_push($bouncedEmailDataForMongo, $email);
        }
    }

    echo "--verifiedddd--".json_encode($verifiedEmailDataForMongo);
    echo "--bouncedddd--".json_encode($bouncedEmailDataForMongo);

//    logDataToMongo($verifiedEmailDataForMongo,$collectionName."_verified");
//    logDataToMongo($bouncedEmailDataForMongo,$collectionName."_bounced");

        exportToCSV($bouncedEmailDataForMongo,$collectionName,$ifBounced);
        exportToCSV($verifiedEmailDataForMongo,$collectionName,$ifBounced);
}

function exportToCSV($mailContent,$collectionName,$ifBounced){
    if(!file_exists ( 'output' )){
     mkdir('output');
    }
    if($ifBounced) {
        $filename = 'output/' . $collectionName . '_bounced.csv';
    } else {
        $filename = 'output/' . $collectionName . '_verified.csv';
    }
    $stream = fopen($filename, 'a');
    foreach ($mailContent as $val) {
        fputcsv($stream, array($val));
    }
    fclose($stream);
}

function logDataToMongo($mailContent,$collectionName){
    $mongoConn = new MongoConnection();
    $conn = $mongoConn->createConnection();
    $db = $mongoConn->selectDatabase($conn);
    $coll = $db->selectCollection($collectionName);
    insertIntoMongo($coll,$mailContent);
    $mongoConn->closeConnection($conn);
}

function insertIntoMongo($coll,$dataArray){
    echo "finalcollectionname--".($coll);
//    print_r($dataArray);
//    $result = $coll->batchInsert($dataArray);
//    echo "---resultttt---".json_encode($result);
}


$channel->close();
$connection->close();

function getRabbitMqConnection(){
    $rabbitMqConnection = new RabbitMqConnection();
    $conn = $rabbitMqConnection->createConnection();
    return $conn;
}


?>