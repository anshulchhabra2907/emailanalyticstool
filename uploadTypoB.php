<?php

require_once 'master_connection_mongo.php';
require_once 'Constants.php';

if($_REQUEST["require"] == "update_typo_list") {

    $collectionName = "master_domain_typo_list";
    $conn = getMongoConnection();
    $conn->setCollection($collectionName);

    $typoDomain = $_REQUEST["typo_domain"];
    $typoDomainList = explode(" ",$typoDomain);

    $reason = $_REQUEST["reason"];
    $canBeDomain =  $_REQUEST["original_domain"];

    $ifDelete = $_REQUEST["is_delete"];

    foreach ($typoDomainList as $domain ){
        $filterArray=array();
        if ($ifDelete) {
            $filterArray=array("typo_domain"=>$domain);
            $res=$conn->remove($filterArray);
        } else {
            $filterArray=array("typo_domain"=>$domain,"reason"=>$reason,"can_be_domain"=>$canBeDomain);
            $res=$conn->insert($filterArray, array('limit' => 0));
        }
        echo "successfully updated";
    }
    exit;
}else if($_REQUEST["require"] == "get_typo_list"){

    ini_set("memory_limit", -1);
    ini_set('max_execution_time', 30000); //30000 seconds = 500 minutes

    $collectionName = "master_domain_typo_list";
    $conn = getMongoConnection();
    $conn->setCollection($collectionName);
    $filterArray=array();
    $mongoData = $conn->find($filterArray, array('limit' => 0));

    $finalDomainData = array();
    foreach ($mongoData as $domainData) {
        $typoDomain = $domainData['typo_domain'];
        $reason = array_keys(Constants::$reason,$domainData['reason']);
        $canBeDomain = $domainData['can_be_domain'];
        array_push($finalDomainData, array($typoDomain, $reason,$canBeDomain));
    }
    if(count($finalDomainData)==0){
        echo 0;
        exit;
    }
    echo json_encode($finalDomainData);
    exit;


}

function getMongoConnection(){
    $conn = new MongoConnection();
    return $conn;
}
