<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/5/17
 * Time: 12:00 PM
 */
require_once('messagePriority.php');
require_once('logger.php');

////local
//define("RMQS_HOST","127.0.0.1");
//define("RMQS_PORT","5672");
//
//define("RMQS_USER","guest");
//define("RMQS_PASS","guest");
//define("RMQS_VHOST","");

//staging
define("RMQS_HOST","94.130.9.112");
define("RMQS_PORT","15672");

define("RMQS_USER","gauser");
define("RMQS_PASS","betaout@123");
define("RMQS_VHOST","bov2-durable");


class RabbitMqConnection {

    public static $_queue;
    protected static $_host = RMQS_HOST;
    protected static $_port = RMQS_PORT;
    protected static $_user = RMQS_USER;
    protected static $_password = RMQS_PASS;
    protected static $_vhost = RMQS_VHOST;
    protected static $_consumerTag = "consumer";
    protected static $_queueCounter = 0;
    protected static $_maxQueueCounter = 50000;
    protected static $_idleTimeout = 30000; // in seconds
    protected static $_x_max_priority = 20;
    protected static $_key;
    protected static $_connection = false;
    protected static $_channel = array();
    protected static $_exchange = array();
    protected static $_queueContext = array();
    protected $_no_ack = false;
    protected $_pre_ack = false;
    protected $_msg_delay = false;
    protected $_prefetch_count = 10;
    protected $_debug = true;

    /*     * *
     * @args $message array()
     * $connectionType
     * $json_encode @boolean
     * $delay in milliseconds (1 seconds = 1000 milliseconds)
     */

    public function _produce($message,$queue_name, $connectionType = 'socket', $json_encode = true, $delay = 0) {
        try {
            if (empty($message)) {
                return;
            }
            if (!empty($queue_name)) {
                self::$_queue = $queue_name;
            }

            $delay = (int) $delay;
            $delay = 0;
            global $debugMode;
//            if ($debugMode)
//                _echo("Push Data in Rabbitmq  " . self::$_queue, $message);

            $starttime = microtime(true);

            $priority = RabbitMQ_MessagePriority::_get($message, self::$_queue);

            self::$_key = self::$_queue . '_producer';

            $connection = self::getConnection();
            if (!$connection) {
                self::$_connection = false;
                $connection = self::getConnection();
            }
            if ($connection) {
                $channel = self::createChannel();
                $exchange = self::declareExchange($channel, $this->_msg_delay);
                $queue = self::createQueue($channel);

                if ($json_encode) {
                    $message = $this->_encode($message);
                }

                // try to publish the message on the queue
                try {

//                    $statsdObj = new BOStatsd_V2_Statsd('betaout.rmqs.' . str_replace('.', "-", RMQS_HOST) . '.publish', 'time');
//                    $statsd = $statsdObj->_startTiming();
                    /**
                     * @author Dharmendra Rai<developer.dharam@gmail.com>
                     * @desc Change for message scheduling support
                     */
                    if ($this->_msg_delay) {
//                        'headers'=>array('x-delay' => 90000)
                        $message = $exchange->publish($message, self::$_queue . '_key', AMQP_NOPARAM, array('priority' => $priority, 'delivery_mode' => 2, 'headers' => array('x-delay' => $delay)));
                    } else {
                        $message = $exchange->publish($message, self::$_queue . '_key', AMQP_NOPARAM, array('priority' => $priority, 'delivery_mode' => 2));
                    }

//                    $statsdObj->_endTiming($statsd);
                } catch (AMQPExchangeException $ex) {
//                    print_r($ex->getMessage());
                } catch (AMQPConnectionException $ex) {
//                    print_r($ex->getMessage());
                } catch (AMQPChannelException $ex) {
//                    print_r($ex->getMessage());
                }
            }

            if (defined('ISAPI') && constant('ISAPI')) {
//                $connection->disconnect();
            }

            $timeTaken = microtime(true) - $starttime;
//            echo "/**rmqt_amqp=$timeTaken*/";
        } catch (Exception $ex) {
//            print_r($ex->getMessage());
        }
    }

//    public function __destruct() {
//        self::$_connection->disconnect();
//    }

    /**
     * @author Dharmendra Rai <developer.dharam@gmail.com>
     * @desc  method _encode  only  encode utf-8 format data and log other format data
     * */
    public function _encode($message) {

//       $message1= json_encode($message,JSON_PARTIAL_OUTPUT_ON_ERROR);// may be used to in future for accepting JSON_PARTIAL_OUTPUT_ON_ERROR
        $message1 = json_encode($message);
        if (json_last_error()) {
            $message = $this->utf8ize($message); // convert in utf-8 before log in mongo
            //
            $obj = new RabbitMQ_ApiJsonError();
            $obj->_produce($message);

        }
        return $message1;
    }

    public function processMessage($envelope, $queue) {
        /**
         * @author Jitendra Singh Bhadouria <jeetusb.singh@gmail.com>
         * @desc Send time for each queue process to statsd
         */
//        try {
//            $statsdObj = new BOStatsd_V2_Statsd(RMQS_VHOST . '.betaout.queue', self::$_queue);
//            $statsd = $statsdObj->_startTiming();
//        } catch (Exception $ex) {
//          print_r($ex->getMessage());
//        }
        //message process and acknowledge code.
        $messageBody = $envelope->getBody();
        $message = json_decode($messageBody, true);
        $msgbytes = round((strlen($messageBody) / 1024), 4);

//        log ecommerce activities in a different queue for audit purpose
//        $message=DB_Utility::checkQueue(self::$_queue, $message);
        //resolve identifier parameters to user.
//        $message = $this->_alterMessageForIdentifer($message);


//        echo "\n message array inside base";
//        print_r($message);

        $startTimeConsume = microtime(true);
        $startMemoryUsage = memory_get_usage();
        $queuename = $queue->getName();

        if ($this->_pre_ack) {

            //before acknowledgement
            $queue->ack($envelope->getDeliveryTag());

            $this->_process($message);

            self::$_queueCounter++;
        } else {
            //after acknowledgement

            $this->_process($message);

            $queue->ack($envelope->getDeliveryTag());

            self::$_queueCounter++;
        }

//        $exetime = round((microtime(true) - $startTimeConsume), 4);
//        $memconsumed = round(((memory_get_usage() - $startMemoryUsage) / 1024), 4);
//
//        $arrLog['projectId'] = isset($message['projectId']) ? $message['projectId'] : 0;
//        $arrLog['queuename'] = $queuename;
//        $arrLog['acktype'] = $this->_pre_ack ? 1 : 0;
//        $arrLog['module'] = 'rabbitmq-consume';
//        $arrLog['exetime'] = $exetime;
//        $arrLog['msgbytes'] = $msgbytes;
//        $arrLog['memconsumed'] = $memconsumed;
//        $arrLog['msgcount'] = count($message);
//
//
//        $objLogger = new Logger_Logger();
//        $objLogger->log($arrLog);

//        echo PHP_EOL . "Queue counter " . self::$_queueCounter . " max queue counter " . self::$_maxQueueCounter . PHP_EOL;
//        echo  self::$_queueCounter;
//        if (self::$_queueCounter >= self::$_maxQueueCounter) {
//            echo "consumer_exit" . PHP_EOL;

//            $objLogger->writeLog();
//
//            exit;
//        }


//        after acknowledgement
//        $this->_process($message);
//        $queue->ack($envelope->getDeliveryTag());
//        Send end Time and counter to statsd.
//        try {
//            $statsdObj->_endTiming($statsd);
//            $statsdObj->_count(1);
//        } catch (Exception $ex) {
//            print_r($ex);
//          print_r($ex->getMessage());
//        }
    }

    public function _consume($pid = 0) {
// echo "\n start _consume  inside base";
//        echo RMQS_HOST;
        try {
            self::$_key = self::$_queue . '_consumer';

            $connection = self::getConnection(true);

            if ($connection) {

                $channel = self::createChannel();

                $exchange = self::declareExchange($channel, $this->_msg_delay);

                $channel->setPrefetchCount($this->_prefetch_count);

                $queue = self::createQueue($channel);
            }

//        echo "\n after connection";
//        Consume messages on queue

            $queue->consume(array($this, "processMessage"));
        } catch (Exception $ex) {
            $objLogger = new Logger_Logger();
            $objLogger->writeLog();

            echo $ex->getMessage();
        }


//        echo "\n after consume";
//        while (1) {
//            // Get the message from the queue.
//            while ($envelope = $queue->get()) {
//
//                $message = json_decode($envelope->getBody(), true);
//
//                $statsdObj = new BOStatsd_Statsd('betaout.queue', self::$_queue);
//                $statsd = $statsdObj->_startTiming();
//
//                $this->_process($message);
//
//                $statsdObj->_endTiming($statsd);
//
//                $queue->ack($envelope->getDeliveryTag());
//            }
//        }
    }

    public static function TestConnection() {
        try {
            $startTime = microtime(true);

//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmqs.' . str_replace('.', "-", RMQS_HOST) . '.connection', 'time');
//            $statsd = $statsdObj->_startTiming();

            if (!isset(self::$_connection) || !self::$_connection) {

                self::$_connection = new AMQPConnection(array(
                    'host' => self::$_host,
                    'login' => self::$_user,
                    'password' => self::$_password,
                    'vhost' => self::$_vhost
                ));

                self::$_connection->connect();

                if ((self::$_queue == "user_merge_unprocess_slower") || (self::$_queue == "user_merge_unprocess_slowest")) {
                    self::$_connection->setTimeout(30);
                } else {
                    self::$_connection->setTimeout(300);
                }
            }

            $timeTaken = microtime(true) - $startTime;
        } catch (AMQPException $ex) {

            self::$_connection = false;
//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmq', 'occured');
//            $statsdObj->_count(1);
            // print_r($ex->getMessage());
        }
//        $statsdObj->_endTiming($statsd);

        return self::$_connection;
    }

    private static function getConnection($fromConsumer = false) {
        try {
            $startTime = microtime(true);

//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmqs.' . str_replace('.', "-", RMQS_HOST) . '.connection', 'time');
//            $statsd = $statsdObj->_startTiming();

            if (!isset(self::$_connection) || !self::$_connection) {

                self::$_connection = new AMQPConnection(array(
                    'host' => self::$_host,
                    'login' => self::$_user,
                    'password' => self::$_password,
                    'vhost' => self::$_vhost
                ));

                self::$_connection->connect();

                if ((self::$_queue == "user_merge_unprocess_slower") || (self::$_queue == "user_merge_unprocess_slowest")) {
                    self::$_connection->setTimeout(30);
                } else {
                    self::$_connection->setTimeout(self::$_idleTimeout);
                }
                if ($fromConsumer) {
                    $userMergeQueue = array("user_reg_even", "user_reg_odd", "user_unreg_even", "user_unreg_odd", "server_24_user_merge");
                    if (in_array(self::$_queue, $userMergeQueue)) {
                        self::$_maxQueueCounter = 250;
                        self::$_connection->setTimeout(15);
                    }
                }
            }

            $timeTaken = microtime(true) - $startTime;
        } catch (AMQPException $ex) {

            self::$_connection = false;

//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmq', 'occured');
//            $statsdObj->_count(1);
//            print_r($ex->getMessage());
        }
//        $statsdObj->_endTiming($statsd);

        return self::$_connection;
    }

    private static function createChannel() {
        try {

//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmqs.' . str_replace('.', "-", RMQS_HOST) . '.channel', 'time');
//            $statsd = $statsdObj->_startTiming();

            $startTime = microtime(true);

            if (!isset(self::$_channel[self::$_key])) {

                self::$_channel[self::$_key] = new AMQPChannel(self::$_connection);
            }
            $timeTaken = microtime(true) - $startTime;
        } catch (AMQPConnectionException $ex) {
//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmq', 'occured');
//            $statsdObj->_count(1);
//            print_r($ex->getMessage());
        }

//        $statsdObj->_endTiming($statsd);

        return self::$_channel[self::$_key];
    }

    private static function declareExchange($channel, $delay = FALSE) {
        try {

//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmqs.' . str_replace('.', "-", RMQS_HOST) . '.exchange', 'time');
//            $statsd = $statsdObj->_startTiming();
            // declare the exchange

            $startTime = microtime(true);
            if (!isset(self::$_exchange[self::$_key])) {

                self::$_exchange[self::$_key] = new AMQPExchange($channel);
                self::$_exchange[self::$_key]->setName(self::$_queue . '_exchange');

                if ($delay) {
                    /**
                     * @author Dharmendra Rai<developer.dharam@gmail.com>
                     * @desc Change for message scheduling support
                     */
                    self::$_exchange[self::$_key]->setArguments(array("x-delayed-type" => "direct"));

                    self::$_exchange[self::$_key]->setFlags(AMQP_DURABLE);

                    self::$_exchange[self::$_key]->setType('x-delayed-message');
                } else {
                    self::$_exchange[self::$_key]->setType('direct');
                }
                self::$_exchange[self::$_key]->declareExchange();
            }

            $timeTaken = microtime(true) - $startTime;
        } catch (AMQPExchangeException $ex) {
//            print_r($ex->getMessage());
        } catch (AMQPConnectionException $ex) {
//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmq', 'occured');
//            $statsdObj->_count(1);
//            print_r($ex->getMessage());
        }
//        $statsdObj->_endTiming($statsd);

        return self::$_exchange[self::$_key];
    }

    private static function createQueue($channel) {
        // try to create the queue
        try {

//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmqs.' . str_replace('.', "-", RMQS_HOST) . '.queue', 'time');
//            $statsd = $statsdObj->_startTiming();

            // create the queue
            $startTime = microtime(true);

            if (!isset(self::$_queueContext[self::$_key])) {

                self::$_queueContext[self::$_key] = new AMQPQueue($channel);

                self::$_queueContext[self::$_key]->setName(self::$_queue);

                self::$_queueContext[self::$_key]->setArgument('x-max-priority', self::$_x_max_priority);

                self::$_queueContext[self::$_key]->setFlags(AMQP_DURABLE);

                self::$_queueContext[self::$_key]->declareQueue();

                self::$_queueContext[self::$_key]->bind(self::$_queue . '_exchange', self::$_queue . '_key');
            }

            $timeTaken = microtime(true) - $startTime;
        } catch (AMQPQueueException $ex) {
//            $statsdObj = new BOStatsd_V2_Statsd('betaout.rmq', 'occured');
//            $statsdObj->_count(1);
//            print_r($ex->getMessage());
//            $log = Log::getInstance();
//            $log->forcedLog('', '', Log4Php_LoggerLevel::getLevelError(), $ex->getMessage() . PHP_EOL . $ex->getTraceAsString());
        } catch (AMQPConnectionException $ex) {
//            print_r($ex->getMessage());
//            $log = Log::getInstance();
//            $log->forcedLog('', '', Log4Php_LoggerLevel::getLevelError(), $ex->getMessage() . PHP_EOL . $ex->getTraceAsString());
        }

//        $statsdObj->_endTiming($statsd);
        return self::$_queueContext[self::$_key];
    }

    public function setQueueName($queue) {
        self::$_queue = $queue;
    }

    public function _resetConnection($key) {
        self::$_connection = self::$_channel[self::$_key] = self::$_exchange[self::$_key] = self::$_queueContext[self::$_key] = false;
    }

    /**
     * Here the original message will be retreived
     * Each extended class must overwrite it to change the functionality.
     */
    protected function _process($message) {

    }

    /**
     * @author Dharmendra Rai <developer.dharam@gmail.com>
     * @desc  method utf8ize  will encode all array and string in utf-8
     * */
    private function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = $this->utf8ize($v);
            }
        } else if (is_string($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

    /**
     * @author Dharmendra Rai <developer.dharam@gmail.com>
     * @desc  will retrieve identifiers and set userCache after processing.
     * */
    private function _alterMessageForIdentifer($message) {
        $userCacheKey = Identifier_ReservedKeys::_USER_CACHE_KEY;
        if (!isset($message[$userCacheKey])) {
            if (isset($message['identifiers']) && isset($message['projectId'])) {
                $timestamp = isset($message['timestamp']) ? $message['timestamp'] : false;
                $obj = new Identifier_User($message['projectId'], true, true);
                $message[$userCacheKey] = $obj->_getUserData($message['identifiers'], $timestamp);
                unset($message['identifiers']);
                if (isset($message[$userCacheKey]['userId'])) {
                    $redisCacheObj = new Redis_UserCacheManager($message['projectId'], $message[$userCacheKey]['userId']);
                    $userInfo = $redisCacheObj->_getUserCacheById();
                    $userInfoMerge = array_merge($userInfo, $message[$userCacheKey]);
                    $message[$userCacheKey] = $userInfoMerge;
                }
            }
        }
        return $message;
    }

}

