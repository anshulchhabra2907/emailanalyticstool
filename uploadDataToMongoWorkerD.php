<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 8/5/17
 * Time: 7:32 PM
 */

require_once('master_connection_rabbitmq.php');
require_once('master_connection_mongo.php');
require_once('logger.php');


class UploadDataToMongoWorkerD extends RabbitMqConnection
{

    public function __construct($queue)
    {
        parent::$_queue = $queue;
    }

    protected function _process($mailData)
    {
        try {
            $collectionName = $mailData['collection_name'];
            $mailList = $mailData['mail_list'];
            $ifLast = $mailData['end'];
            $this->logEmailListInDb($mailList, $collectionName,$ifLast);
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    function logEmailListInDb($emailList, $collectionName,$ifLast)
    {
        echo json_encode($emailList);
        $conn = $this->getMongoConnection();
        $conn->setCollection($collectionName);
        $response = $conn->batchInsert($emailList, array('ordered' => false), 1);
        if($ifLast=="1"){
//            $this->sendMail("Data Uploaded to Mongo",$collectionName);
        }
    }

    function getMongoConnection()
    {
        $conn = new MongoConnection();
        return $conn;
    }

    function sendMail($message,$collection_name){

        $headers = "From:EMAIL_REPORT_UPLOAD <betaout.errors@gmail.com>\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";
        $headers .= "Content-Transfer-Encoding:base64 \r\n";
        $messagebody= $base64contents = rtrim(chunk_split(base64_encode($message)));

        mail("betaout.errors@gmail.com,anshul@betaoutinc.com", "EMAIL_REPORT_UPLOAD COLLECTION:".$collection_name , $messagebody, $headers);

    }

}
