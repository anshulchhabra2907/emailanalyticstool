<?php

require_once 'master_connection_mongo.php';

if($_REQUEST["require"] == "csv") {

    $collectionName = $_REQUEST["coll_name"];

    $conn = getMongoConnection();
    $conn->setCollection($collectionName);

    $toFilter = $_REQUEST["to_filter"];
    $ifDownload = $_REQUEST["if_download"];

    if ($toFilter == 'all') {
        $filterArray = array();
    } elseif ($toFilter == 'verified') {
        $filterArray = array("result" => "verified");
    } elseif ($toFilter == 'bounced') {
        $filterArray = array("result" => "bounced");
    } elseif ($toFilter == 'risky') {
        $filterArray = array("result" => "risky");
    } elseif ($toFilter == 'toverify') {
        $conn->setCollection($collectionName . "_toverify");
        $filterArray = array();
    }

    $count = $conn->count($filterArray);

    if ($ifDownload == 'count') {
        echo $count;
        exit;
    } else {
        if ($count == 0) {
            echo "fail";
        } else {
            ini_set("memory_limit", -1);
            ini_set('max_execution_time', 30000); //30000 seconds = 500 minutes
            $emailInfo["email_id"] = '';
            $emailInfo["result"] = '';
            $emailInfo["reason"] = '';
            $emailInfo["typo"] = '';
            $emailInfo["mx"] = '';
            $emailInfo["disposable"] = '';
            $emailInfo["smtp"] = '';
            $emailInfo["did_you_mean"] = '';
            $emailInfo["score"] = '';
            $emailInfo["user"] = '';
            $emailInfo["domain"] = '';

            $mongoData = $conn->find($filterArray, array('limit' => 0));

            $finalMailData = array(array("email", "result", "reason", "typo", "mx", "disposable", "smtp", "did_you_mean",
                "score", "user", "domain"));

            foreach ($mongoData as $mailData) {
                $emailId = $mailData['email_id'];
                $result = $mailData['result'];
                $reason = $mailData['reason'];
                $typo = $mailData['typo'];
                $mx = $mailData['mx'];
                $disposable = $mailData['disposable'];
                $smtp = $mailData['smtp'];
                $did_you_mean = $mailData['did_you_mean'];
                $score = $mailData['score'];
                $user = $mailData['user'];
                $domain = $mailData['domain'];
                array_push($finalMailData, array($emailId, $result, $reason, $typo, $mx, $disposable, $smtp, $did_you_mean,
                    $score, $user, $domain));//2nd.3rd.4th....rows..
            }
            echo json_encode($finalMailData);
            exit;
        }
        exit;
    }
}

function getMongoConnection(){
    $conn = new MongoConnection();
    return $conn;
}

//FOR DOWNLOADING CSV FROM PHP
//$delimiter = ',';
//$enclosure = '"';
//$filename = "test";
//$filepath = $_SERVER["DOCUMENT_ROOT"] . $filename.'.csv';
//header('Content-Description: File Transfer');
//header('Content-Type: application/octet-stream');
//header('Content-Disposition: attachment; filename='.basename($filepath));
//header('Expires: 0');
//header('Cache-Control: must-revalidate');
//header('Pragma: public');
//header('Content-Length: ' . filesize($filepath));
//$output = fopen($filepath, 'w+');
//fputcsv($output, array("Number", "Descriptiona", "test"), $delimiter, $enclosure);
//fputcsv($output, array("100", "testDescriptiona", "10"), $delimiter, $enclosure);
//fputcsv($output, array("100", "testDescriptiona", "10"), $delimiter, $enclosure);
//fputcsv($output, array("100", "testDescriptiona", "10"), $delimiter, $enclosure);
//fputcsv($output, array("100", "testDescriptiona", "10"), $delimiter, $enclosure);
//fclose($output);
//readfile($filepath);
//exit;
