<?php
/**
 * Created by PhpStorm.
 * User: Anshul
 * Date: 14/4/17
 * Time: 3:55 PM
 */
require_once('logger.php');

define("DB_NAME","mailAnayticLog");
define("MONGO_IP","127.0.0.1");
define("MONGO_PORT","27017");
define("COLLECTION","verified_mail_list_master");
//define("IP","mongodb://stag-push.betaout.in:27017");

class MongoConnection {

//    protected $MONGO_DATABASE = MONGO_BO_DB; // define this name in child/inherited class
//    protected $collection = 'test_123'; //  define this name in child/inherited class
    protected $MONGO_DATABASE = DB_NAME;
    protected $_collection = COLLECTION;
    protected $MONGO_HOST = MONGO_IP; // This can be redefine in child class in case of diffrent server
    protected $MONGO_PORT = MONGO_PORT;
    protected $MONGO_TIMEOUT = '';
    protected $MONGO_USER = '';
    protected $MONGO_PSWD = '';
    protected $MONGO_AUTH = '';
    protected $_options = array();
    protected static $mongoConnection;
    public $bulk_up_obj;
    protected $_writeConcern = 0;
//    protected static $rp;
//    protected static $wp;
    protected $_bulk_inserted_ids;

    /**
     * (Betaout Mongo connector)<br/>
     * Constructs and maintain a mongodb Persistent Connections
     */
    public function __construct() {
        try {
            if (empty($this->MONGO_DATABASE)) {
                throw new Exception('Please define Database name in your class');
            }
            $_EP = "mongodb://" . $this->MONGO_HOST . ':' . $this->MONGO_PORT . "/" . $this->MONGO_DATABASE;
            $options['socketTimeoutMS'] = $this->MONGO_TIMEOUT;
            $options['db'] = $this->MONGO_DATABASE;
            $options['connectTimeoutMS'] = $this->MONGO_TIMEOUT;
            if ($this->MONGO_AUTH) {
                $options['username'] = $this->MONGO_USER;
                $options['password'] = $this->MONGO_PSWD;
            }

            if (!isset(self::$mongoConnection)) {
                self::$mongoConnection = new \MongoDB\Driver\Manager($_EP, $options);
//                self::$rp = new \MongoDB\Driver\ReadPreference(\MongoDB\Driver\ReadPreference::RP_SECONDARY_PREFERRED);
//                self::$wp = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
//                self::$wp = new MongoDB\Driver\WriteConcern(0, 1000);
            }
            if (empty($this->_collection)) {
                throw new Exception('Please define collection name in your class');
            }
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    /**
     * Inserts batch of documents into the collection
     * @param array $a <p>
     * An array of arrays or objects.
     * </p>
     * @return array an array containing the status of the  insert and update
     * @throws Exception
     */
    public function batchInsert($mongoBatchArray, $options = array(),$w=0)
    {
        try {
            $newResult = array();
            $this->_bulk_init($options);
            foreach ($mongoBatchArray as $doc) {
                $this->_add($doc);
            }
            // execute bulk
            $bulk_resp = $this->_bulk_execute($w);
            if (is_object($bulk_resp)) {

                $newResult['nInserted'] = $bulk_resp->getInsertedCount();
                $newResult['nMatched'] = $bulk_resp->getMatchedCount();
                $newResult['nModified'] = $bulk_resp->getModifiedCount();
                $newResult['nRemoved'] = $bulk_resp->getDeletedCount();
                $newResult['nUpserted'] = $bulk_resp->getUpsertedCount();
                $newResult['upsertedIds'] = $bulk_resp->getUpsertedIds();
                $newResult['writeErrors'] = $bulk_resp->getWriteErrors();
                $newResult['writeConcernError'] = $bulk_resp->getWriteConcernError();
                $newResult['newInsertedIds'] = $this->_bulk_inserted_ids;
            }

            return $newResult;
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    /**
     * Inserts a document into the collection
     * @param array $doc <p>
     * An array of single document
     * </p>
     * @return int $mongoid or false
     * @throws Exception
     */
    public function insert($doc = array()) { // use for single or bulk insert.
        try {
            $this->_bulk_init();
            if (is_array($doc)) {
                $new_mongo_id = $this->_add($doc);
            } else {
                throw new Exception('Invalid format, It should be associative array');
            }
            // execute bulk
            $bulk_resp = $this->_bulk_execute();

            if ($bulk_resp->getInsertedCount())
                return $new_mongo_id;
            else {
                // need to write find code in case dublicate key  error
                return FALSE;
            }
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    /**
     * (Betaout Mongodb <br/>
     * Queries this collection, returning a <b>MongoCursor</b>
    for the result set
     * @link http://php.net/manual/en/mongocollection.find.php
     * @param array $query [optional] <p>
     * The fields for which to search. MongoDB's query language is quite
     * extensive. The PHP driver will in almost all cases pass the query
     * straight through to the server, so reading the MongoDB core docs on
     * find is a good idea.
     * array('x' => array('$gt' => 1));
     * </p>
     * <p>
     * Please make sure that for all special query operators (starting with
     * $) you use single quotes so that PHP doesn't try to
     * replace "$exists" with the value of the variable
     * $exists.
     * </p>
     * @param array $queryOptions [optional] <p>
     * Filter fields and limit  or sor
     * array(projection' => ['_id' => 0],'sort' => ['x' => -1],'limit' => 10).
     * </p>
     * @param boolean $count [optional] <p>
     * return total number of documents matching the query.
     *  </p>
     * @return Records as array.
     */
    public function find($filter = array(), $queryOptions = array('limit' => 10), $count = false, $rp = 1) {
        try {

//        $filter = ['x' => ['$gt' => 1]];
//        $options = [
//            'projection' => ['_id' => 0],
//            'sort' => ['x' => -1],
//            'limit' => 10,
//            'skip' =>20
//        ];


            $query = new MongoDB\Driver\Query($filter, $queryOptions);
            $cursor = self::$mongoConnection->executeQuery($this->MONGO_DATABASE . "." . $this->_collection, $query, $this->_getReadPreference($rp));
            $cursor->setTypeMap(['root' => 'array', 'document' => 'array', 'array' => 'array']);
            if ($count)
                return array("data" => $cursor->toArray(), "count" => $this->count($query));
            else {
                return $cursor->toArray();
            }
        } catch (Exception $ex) {
            print_r($ex);
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    /**
     * Counts the number of documents in this collection
     * @param array $query [optional] <p>
     * Associative array or object with fields to match.
     * </p>
     * @return int Total number of documents matching the query.
     */
    public function count($query = array()) {
        try {
            $command = new MongoDB\Driver\Command(["count" => $this->_collection, "query" => $query]);
            $result = self::$mongoConnection->executeCommand($this->MONGO_DATABASE, $command);
            $res = current($result->toArray());
            $count = $res->n;
            return $count;
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    public function _getReadPreference($pref = 1) {
        switch ($pref) {
            case 0:
                $rp = new \MongoDB\Driver\ReadPreference(\MongoDB\Driver\ReadPreference::RP_SECONDARY);

                break;

            case 1:
                $rp = new \MongoDB\Driver\ReadPreference(\MongoDB\Driver\ReadPreference::RP_SECONDARY_PREFERRED);

                break;
            case 2:
                $rp = new \MongoDB\Driver\ReadPreference(\MongoDB\Driver\ReadPreference::RP_PRIMARY_PREFERRED);

                break;
            case 3:
                $rp = new \MongoDB\Driver\ReadPreference(\MongoDB\Driver\ReadPreference::RP_PRIMARY);
                break;

            default:
                break;
        }

        return $rp;
    }

    //For batch update------------------------------------------------------------------------------------------------------->
    public function _bulk_init($options=array()) {
        try {
            $this->bulk_up_obj = new MongoDB\Driver\BulkWrite($options);
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . __FUNCTION__);
        }
    }

    public function _add($doc) {
        try {
            if (!isset($this->bulk_up_obj)) {
                $this->_bulk_init();
            }
            $resp = $this->bulk_up_obj->insert($doc);

            $new_mongo_id = "";
            foreach ($resp as $res) {
                $new_mongo_id = $res;
            }
            if (!empty($new_mongo_id)) {
                $doc['_id'] = $new_mongo_id;
                $this->_bulk_inserted_ids[$new_mongo_id] = $doc;
            }
            return $new_mongo_id;
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    public function _bulk_add($match = array(), $update = array() , $upsert = array('upsert' => true, 'multi' => true) , $options=array()) {
        try {
            if (!isset($this->bulk_up_obj)) {
                $this->_bulk_init();
            }
            $this->bulk_up_obj->update($match, $update, $options);
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }


//        $this->bulk_up_obj->add((object) $update);
    }

    /**
     *
     * @param type $match
     * @param type $update
     * @param type $upsert
     */
    public function update($match = array(), $update = array(), $options = array('upsert' => true, 'multi' => true)) {
        try {
            if (!isset($this->bulk_up_obj)) {
                $this->_bulk_init();
            }
            $this->bulk_up_obj->update($match, $update, $options);
            return $this->_bulk_execute();
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }


    /**
     * Final execute the query on collection
     * @param int $w <p>
     * write concer value in number.
     * </p>
     * @return array an array containing the status of the  insert and update
     * @throws Exception
     */
    public function _bulk_execute($w = 0) {
        try {
            $wp = new MongoDB\Driver\WriteConcern($w, 1000000);
            $res = self::$mongoConnection->executeBulkWrite($this->MONGO_DATABASE . "." . $this->_collection, $this->bulk_up_obj, $wp);
            return $res;
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    public function remove($criteria, $options = array('limit' => 1)) {
        try {

//            if (!isset($this->bulk_up_obj)) {
                $this->_bulk_init();
//            }
            $this->bulk_up_obj->delete($criteria, $options);
            $bulk_resp = $this->_bulk_execute();
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    public function aggregate($aggregate) {
        try {
            throw new Exception("Now, we planning to remove all aggregation and group query from mongodb");
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

    public function setCollection($collection) {
        try {
            if (empty($collection)) {
                throw new Exception('Missing Collection Name');
            }
            $this->_collection = $collection;
        } catch (Exception $ex) {
            Logger_Logger::exception($ex, __CLASS__ . ':' . __FUNCTION__);
        }
    }

//    function __destruct() {
//        $this->writeLog();
//    }
}